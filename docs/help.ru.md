---
description: Узнайте, как получить помощь с mastercomfig.
...

# Помощь

Нужна помощь с mastercomfig?

Ознакомьтесь с этими ресурсами:

* [Руководство по быстрым исправлениям](next_steps/quick_fixes.md)
* [Инструкции по установке](setup/clean_up.md)

Все еще не можете найти ответ и хотите попросить о помощи?

* [Спросите в #comfig-help на Discord](https://discord.gg/CuPb2zV)
* [Создать тему помощи в Steam](https://steamcommunity.com/groups/comfig/discussions/0/)
